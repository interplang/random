#![allow(non_snake_case)]
#![allow(unused_variables)]

use interplang::vm::value::Value;
use interplang::parser::loc::Loc;
use rand::Rng;

#[no_mangle]
pub fn Interp_Random_Int(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    Value::Int(rand::random::<i64>())
}

#[no_mangle]
pub fn Interp_Random_Float(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    Value::Float(rand::random::<f64>())
}

#[no_mangle]
pub fn Interp_Random_IntRange(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    let Value::Int(lower) = args[0].0.clone() else {
        panic!();
    };
    let Value::Int(upper) = args[1].0.clone() else {
        panic!();
    };
    Value::Int(rand::thread_rng().gen_range(lower..upper))
}

#[no_mangle]
pub fn Interp_Random_FloatRange(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    let Value::Float(lower) = args[0].0.clone() else {
        panic!();
    };
    let Value::Float(upper) = args[1].0.clone() else {
        panic!();
    };
    Value::Float(rand::thread_rng().gen_range(lower..upper))
}

#[no_mangle]
pub fn Interp_Random_IntDataset(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    let Value::Int(size) = args[0].0.clone() else {
        panic!();
    };
    let size = size as usize;
    let mut rng = rand::thread_rng();
    let mut vec = Vec::with_capacity(size);
    for i in 0..size {
        vec.push(Value::Int(rng.gen()));
    }
    Value::List(vec)
}

#[no_mangle]
pub fn Interp_Random_FloatDataset(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    let Value::Int(size) = args[0].0.clone() else {
        panic!();
    };
    let size = size as usize;
    let mut rng = rand::thread_rng();
    let mut vec = Vec::with_capacity(size);
    for i in 0..size {
        vec.push(Value::Float(rng.gen()));
    }
    Value::List(vec)
}
